# csc-group reproducibility workshop

## status quo - in computer science

![status quo computer science](Total.png)

cf. [Repeatability in Computer Science](http://reproducibility.cs.arizona.edu/)


## status quo - in the csc group
 * less then 10% of the preprints link to the code
 * no central repository or an index for code
 * by now no policy on sharing code both with colleagues or the public

## reusability of code comes with
 * efficiency - no more reinventing of the wheel
 * quality - if someone else can replicate your examples...
 * responsibility - for the institute and your fellow scientists

### we start today by raising awareness
 * and check the numerical tests of most recent MPI-csc preprints as follows
 
1. one corresponding author distributes the code 
2. a small group tries to replicate the results

### and investigate some related issues
 * Is the description in the preprint self-contained?
 * Where can I get the code from?
 * Will it run on my computer?
 * What extra packages do I need to make it run?
 * How do I obtain the reported results? 

## list of preprints -- authors -- fellows

Number | Author | interested people
--- | --- | ---
15-07 | Pawan 	 | Cleo, Alexander, Melina F.
15-06 | Heiko 	 | Hamdullah, Sara, V. Khoromskij
15-05 | Martin S. | Petar, Xinliang, B. Khoromskij
15-04 | Jessi 	 | Jonas, Wei, Max
15-03 | Jan 		 | Christoph, Lihong, Carolin
14-22 | Yongjin 	 | Ulrike, Sergej, Bjoern
14-21 | Patrick 	 | Xindu, Martin K., Max
14-19 | Martin H. | Martin R., Mian, Nico
14-16 | Martin R. | if needed

## Reports and Conclusion
 * 4 teams report on their success
 * how far did they get
 * what were the main difficulties
 * what are the most promising remedies 

## Announcements
 * sharing within the group is easiest
 * there are MPI rules for code that is used in publications 
 * best practice guide to appear
 * form for open source development
